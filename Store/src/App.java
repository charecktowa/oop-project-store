import menu.MainMenu;

public class App {
    public static void main(String[] args) {
        try {
            MainMenu newMenu = new MainMenu();
            newMenu.menu();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}