package menu;

import productmanagment.Stock;
import products.*;

import java.util.InputMismatchException;
import java.util.Scanner;

public final class AddProductMenu {

    private Scanner sc;

    public AddProductMenu() {
        this.sc = new Scanner(System.in);
    }

    public Stock addMovie() {
        while(true) {
            try {
                System.out.print("Ingrese el nombre de la pelicula: ");
                var name = sc.nextLine().toUpperCase();

                System.out.print("Ingrese la descripción de \"" + name + "\": ");
                var desc = sc.nextLine().toUpperCase();

                System.out.print("Ingrese el formato de distribución: ");
                var type = sc.nextLine().toUpperCase();

                System.out.print("Ingrese la clasificación: ");
                var rating = sc.nextLine().toUpperCase();

                System.out.print("Ingrese el SKU: ");
                var sku = sc.nextLine().toUpperCase();

                System.out.print("Ingrese el precio: ");
                var price = sc.nextDouble();

                // here we generate the new object
                // TODO: Create the objects

                var p = new Movie.Builder()
                        .name(name)
                        .SKU(sku)
                        .description(desc)
                        .rating(rating)
                        .type(type)
                        .build();

                return new Stock.Builder()
                        .product(p)
                        .price(price)
                        .entryDay()
                        .build();

            } catch (Exception e) {
                e.printStackTrace();
                sc.nextLine(); // avoid saving the last result
                System.out.println();
            }
        }
    }

    public Stock addMusic() {
        while(true) {
            try {
                System.out.print("Ingrese el nombre del álbum: ");
                var name = sc.nextLine();

                System.out.print("Ingrese el nombre del artista/autor: ");
                var artist = sc.nextLine();

                System.out.print("¿Con cuantas canciones cuenta el álbum?: ");
                var songs = sc.nextInt();
                sc.nextLine();

                System.out.print("Ingrese el año de lanzamiento: ");
                var year = sc.nextInt();
                sc.nextLine();

                System.out.print("Ingrese el formato de distribución: ");
                var type = sc.nextLine();

                //TODO: CHECK IF SKU EXIST
                System.out.print("Ingrese el SKU: ");
                var sku = sc.nextLine();

                System.out.print("Ingrese el precio: ");
                var price = sc.nextDouble();

                // TODO: Create object1
                var p = new Music.Builder()
                        .name(name)
                        .SKU(sku)
                        .artistName(artist)
                        .songs(songs)
                        .releaseDate(year)
                        .type(type)
                        .build();

                return new Stock.Builder()
                        .product(p)
                        .price(price)
                        .entryDay()
                        .build();
            } catch (InputMismatchException e) {
                e.printStackTrace();
                sc.nextLine(); // avoid saving the last result
                System.out.println();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public Stock addVideogame() {
        while(true) {
            try {
                // TODO add missing attributes
                System.out.print("Ingrese el título: ");
                var name = sc.nextLine();

                System.out.print("Ingrese una breve descripción: ");
                var desc = sc.nextLine();

                System.out.print("Ingrese el genéro del título: ");
                var genre = sc.nextLine();

                System.out.println("Clasificaciones:{E, T, M, A RP}\n");
                System.out.print("Enter the rating: ");
                var rating = sc.nextLine();

                System.out.print("Ingrese el SKU: ");
                var sku = sc.nextLine();

                System.out.print("Ingrese el precio: ");
                var price = sc.nextDouble();

                // TODO: Create object
                var p = new Videogame.Builder()
                        .name(name)
                        .SKU(sku)
                        .description(desc)
                        .genre(genre)
                        .rating(rating)
                        .build();

                return new Stock.Builder()
                        .product(p)
                        .price(price)
                        .entryDay()
                        .build();
            } catch (InputMismatchException e) {
                e.printStackTrace();
                sc.nextLine(); // avoid saving the last result
                System.out.println();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public Stock addHeadphones() {
        while(true) {
            try {
                System.out.print("Ingrese la marca y modelo de los audifonos: ");
                var name = sc.nextLine();

                System.out.print("Ingrese la freq. minima: ");
                var min = sc.nextInt();

                System.out.print("Ingrese la freq. máxima: ");
                var max = sc.nextInt();

                System.out.print("Ingrese la impedancia de los audifonos: ");
                var imp = sc.nextInt();

                System.out.print("Ingrese los dB del producto: ");
                var dB = sc.nextInt();
                sc.nextLine();

                System.out.print("Ingrese el tipo de audifonos{HEADPHONES, EARPHONES, BLUETOOTH}: ");
                var type = sc.nextLine();

                System.out.print("Ingrese el SKU: ");
                var sku = sc.nextLine();

                System.out.print("Ingrese el precio: ");
                var price = sc.nextDouble();

                var p = new Headphones.Builder()
                        .name(name)
                        .SKU(sku)
                        .minFreq(min)
                        .maxFreq(max)
                        .impedance(imp)
                        .decibel(dB)
                        .type(type)
                        .build();

                return new Stock.Builder()
                        .product(p)
                        .price(price)
                        .entryDay()
                        .build();
            } catch (InputMismatchException e) {
                e.printStackTrace();
                sc.nextLine(); // avoid saving the last result
                System.out.println();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public Stock addSpeakers() {
        while(true) {
            try {
                System.out.print("Ingrese el nombre de los altavoces: ");
                var name = sc.nextLine();

                System.out.print("Ingrese la potencia: ");
                var pow = sc.nextInt();

                System.out.print("Ingrese el peso del producto: ");
                var weight = sc.nextInt();
                sc.nextLine();

                System.out.print("Ingrese el tipo de producto{NORMAL, PORTABLE, BLUETOOTH}: ");
                var type = sc.nextLine();

                System.out.print("Ingrese el SKU: ");
                var sku = sc.nextLine();

                System.out.print("Ingrese el precio: ");
                var price = sc.nextDouble();

                //TODO: Create object
                var p = new Speakers.Builder()
                        .name(name)
                        .SKU(sku)
                        .weight(weight)
                        .power(pow)
                        .type(type)
                        .build();

                return new Stock.Builder()
                        .product(p)
                        .price(price)
                        .entryDay()
                        .build();
            } catch (InputMismatchException e) {
                e.printStackTrace();
                sc.nextLine(); // avoid saving the last result
                System.out.println();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public Stock addTurntable() {
        while (true) {
            try {
                System.out.print("Ingrese el nombre del producto: ");
                var name = sc.nextLine();

                System.out.print("Ingrese la cantidad de velocidades: ");
                var speeds = sc.nextInt();
                sc.nextLine();

                System.out.print("Ingrese el tipo de producto{NEW, VINTAGE}: ");
                var type = sc.nextLine();

                System.out.print("Ingrese el precio del producto: ");
                var price = sc.nextDouble();
                sc.nextLine();

                System.out.print("Ingrese el sku del producto: ");
                var sku = sc.nextLine();

                var p = new Turntable.Builder()
                        .name(name)
                        .SKU(sku)
                        .speeds(speeds)
                        .type(type)
                        .build();

                return new Stock.Builder()
                        .product(p)
                        .price(price)
                        .entryDay()
                        .build();
            } catch (InputMismatchException e) {
                e.printStackTrace();
                sc.nextLine(); // avoid saving the last result
                System.out.println();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

