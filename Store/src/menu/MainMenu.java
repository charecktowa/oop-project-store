package menu;

import java.util.Scanner;

import org.joda.time.DateTime;

import productmanagment.Inventory;

public class MainMenu {

    private DateTime dt;
    private Scanner sc;
    public Inventory inventory;

    public MainMenu() {
        this.dt = new DateTime();
        this.sc = new Scanner(System.in);
        this.inventory = new Inventory();
    }

    public void menu() {

        System.out.println("Son las " + dt.getHourOfDay() + "hrs.");
        System.out.print("¡" + getGreeting() + System.getProperty("user.name") + "!\n\n");

        while (true) {
            try {
                System.out.println("1. Alta Productos "); // C {new, modify quantity}
                System.out.println("2. Buscar Productos"); // R {all, sku}
                System.out.println("3. Baja Producto");
                System.out.println("4. Venta Producto");
                System.out.println("5. Reporte General");
                System.out.println("6. Salir");


                System.out.print("¿Qué desea hacer?: ");
                var option = sc.nextInt();

                switch (option) {
                    case 1 -> addProduct();
                    case 2 -> findProduct(); // i think it is ok, now
                    case 3 -> removeProduct();
                    case 4 -> saleProduct(); // find per SKU
                    case 5 -> generateReport(); // maybe a list where there are only the sold prodcuts (?
                    case 0, -1, 6 -> close();
                }
            } catch (Exception e) {e.fillInStackTrace();}
        }
    }

    private void addProduct() { 
        try {
            System.out.println("¿Qué tipo de producto desea agregar?: ");
            System.out.println("1. Película");
            System.out.println("2. Música");
            System.out.println("3. Videojuego");
            System.out.println("4. Audifonos");
            System.out.println("5. Bocinas");
            System.out.println("6. Tornamesas");

            System.out.print("Opción: ");
            var option = sc.nextInt();

            AddProductMenu a = new AddProductMenu();

            switch(option) {
                case 1 -> inventory.addProduct(a.addMovie());
                case 2 -> inventory.addProduct(a.addMusic());
                case 3 -> inventory.addProduct(a.addVideogame());
                case 4 -> inventory.addProduct(a.addHeadphones());
                case 5 -> inventory.addProduct(a.addSpeakers());
                case 6 -> inventory.addProduct(a.addTurntable());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void findProduct() throws NoSuchFieldException {
        System.out.println("1. Mostrar todos los productos.");
        System.out.println("2. Buscar por SKU.");
        System.out.print("¿Qué desea buscar?: ");
        var option = sc.nextInt();
        sc.nextLine();

        switch (option) {
            case 1 -> inventory.printProducts();
            case 2 -> {
                System.out.println("Ingrese el SKU a buscar: ");
                var sku = sc.nextLine();
             //   inventory.printProducts(sku);
            }
        }
    }

    private void removeProduct() { }

    /**
     * 1. First ask what product wants to sell
     * 2. Find if the product exist (findProduct(SKU))
     * 3. If the SKU exist, check if there are any existence (LinkedList)
     *   3.1 Ask the existences to sell.
     * 4. If there are the amount of existences, change the quantity (LinkedList)
     *
     * 5. If everything is correct, ask if the user wants a report (generateReport())
     *   5.1 Generate report to the console
     * 6. If user did not wanted the report add to a new List designed only for the sale products
     */
    private void saleProduct() { }

    private void generateReport() { }

    private boolean close() { System.exit(0); return true; }

    private String getGreeting() {
        String message;

        if (dt.getHourOfDay() >= 0 && dt.getHourOfDay() <= 11) {
            message = "Buenos dias, ";
        } else if (dt.getHourOfDay() >= 12 && dt.getHourOfDay() <= 18) {
            message = "Buenas tardes, ";
        } else {
            message = "Buenas noches, ";
        }

        return message;
    }
}
