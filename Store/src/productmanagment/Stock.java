package productmanagment;

import org.joda.money.Money;
import org.joda.time.DateTime;
import products.Product;

import java.util.Objects;

public final class Stock {
    private final Product product;
    private final Money price;
    private final DateTime entryDay;

    public Product getProduct() {
        return product;
    }

    public static class Builder {
        private Product product;
        private Money price;
        private DateTime entryDay;

        public Builder product(Product p) throws Exception {
            if (p == null) {
                throw new Exception("I don't know how this happen.");
            }

            this.product = p;
            return this;
        }

        public Builder price(double price) {
            if (price < 0) {
                throw new ArithmeticException("Money isn't negative");
            }

            this.price = Money.parse(("USD" + price));
            return this;
        }

        public Builder entryDay() {
            this.entryDay = new DateTime();
            return this;
        }

        public Builder entryDay(String dt) {
            this.entryDay = new DateTime(dt);
            return this;
        }

        public Stock build() {
            return new Stock(this);
        }
    }

    private Stock(Builder builder) {
        this.product = builder.product;
        this.price = builder.price;
        this.entryDay = builder.entryDay;
    }

    @Override
    public String toString() {
        return ("""
                %s
                Price: %s
                Entry Date: %s
                """).formatted(product.toString(), price, entryDay);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Stock stock = (Stock) o;
        return product.getSKU().equals(stock.product.getSKU());
    }

    @Override
    public int hashCode() {
        return Objects.hash(product, price, entryDay);
    }
}