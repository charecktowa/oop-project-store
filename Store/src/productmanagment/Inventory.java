package productmanagment;

import products.Product;

import java.util.LinkedHashMap;

public final class Inventory {
    LinkedHashMap<Stock, Integer> items;

    public Inventory() {
        this.items = new LinkedHashMap<>();
    }

    public void addProduct(Stock s) {
        if (isKeyPresent(s)) {
            items.put(s, items.get(s) + 1);
        } else {
            items.put(s, 1);
        }
    }

    public void printProducts() throws NoSuchFieldException {
        if (items.isEmpty()) {
            throw new NoSuchFieldException("The list is empty.");
        }

        items.forEach((k, v) -> {
            System.out.println("key: " + k + "value: " + v);
        });
    }

    /**
     *
     * @param s the product to find
     * @return if the product is already in the LinkedHashmap
     */
    public boolean isKeyPresent(Stock s) {
        return items.containsKey(s);
    }
}
