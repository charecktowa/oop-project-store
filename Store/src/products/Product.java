package products;

import java.util.Objects;

public abstract class Product {
    public final String name;
    public final String SKU;

    public String getSKU() {
        return SKU;
    }

    public abstract static class Builder<T extends Builder<T>> {
        private String name;
        private String SKU;

        public T name(String name) {
            if (name == null || name.isEmpty()) {
                throw new IllegalArgumentException("Name product must not be empty.");
            }

            this.name = name;

            return self();
        }

        public T SKU(String SKU) {
            if (SKU == null || SKU.isEmpty()) {
                throw new IllegalArgumentException("SKU must ve valid.");
            }

            this.SKU = SKU;

            return self();
        }

        abstract Product build();

        // para poder sobrescribirlas en las subclases y puedan regresar this
        protected abstract T self();
    }

    public Product(Builder<?> builder) {
        this.name = builder.name;
        this.SKU = builder.SKU;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return name.equals(product.name) && SKU.equals(product.SKU);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, SKU);
    }
}