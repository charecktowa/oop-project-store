package products;

public final class Headphones extends Product {
    
    public enum HeadphonesType {
        HEADPHONES, EARPHONES, BLUETOOTH;

        public static boolean isMember(String type) {
            for(HeadphonesType is: HeadphonesType.values()) {
                if (is.name().equals(type)) {
                    return true;
                }
            }

            return false;
        }
    }

    private final int minFreq;
    private final int maxFreq;
    private final int impedance;
    private final int dB;
    private final HeadphonesType type;

    public static class Builder extends Product.Builder<Builder> {
        private int minFreq;
        private int maxFreq;
        private int impedance;
        private int dB;
        private HeadphonesType type;

        public Builder minFreq(int freq) {
            if (freq < 0) {
                throw new ArithmeticException("Min. Frequency cannot be negative or zero");
            }

            this.minFreq = freq;
            return this;
        }

        public Builder maxFreq(int freq) {
            if (freq <= 0) {
                throw new ArithmeticException("Max. Frequency cannot be negative or zero");
            }

            this.maxFreq = freq;
            return this;
        }

        public Builder impedance(int impedance) {
            if (impedance <= 0) {
                throw new ArithmeticException("Impedance can not be zero or negative.");
            }

            this.impedance = impedance;
            return this;
        }

        public Builder decibel(int dB) {
            if (dB < 0) {
                throw new ArithmeticException("Decibel field can not be zero or less.");
            }

            this.dB = dB;
            return this;
        }

        public Builder type(String type) throws Exception {
            if (!HeadphonesType.isMember(type)) {
                throw new Exception("Invalid type entered.");
            }

            this.type = HeadphonesType.valueOf(type);

            return this;
        }

        @Override
        public Headphones build() {
            return new Headphones(this);
        }

        @Override
        protected Builder self() {
            return this;
        }
    }
    
    private Headphones(Builder builder) {
        super(builder);

        this.minFreq = builder.minFreq;
        this.maxFreq = builder.maxFreq;
        this.impedance = builder.impedance;
        this.dB = builder.dB;
        this.type = builder.type;
    }

    @Override
    public String toString() {
        return ("""
                Name: %s
                Frequency: %d Hz - %d Hz
                Impedance: %d Ω
                Decibels: %d
                Type: %s.
                
                SKU: %s
                """).formatted(name, minFreq, maxFreq, impedance, dB, type, SKU);
    }
}
