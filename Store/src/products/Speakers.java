package products;

public final class Speakers extends Product {

    public enum SpeakersType {
        NORMAL, PORTABLE, BLUETOOTH;


        public static boolean isMember(String type) {
            for(SpeakersType is: SpeakersType.values()) {
                if (is.name().equals(type)) {
                    return true;
                }
            }

            return false;
        }
    }

    private final int weight;
    private final int power;
    private final SpeakersType type;

    public static class Builder extends Product.Builder<Builder> {
        private int weight;
        private int power;
        private SpeakersType type;

        public Builder weight(int weight) {
            if (weight < 0) {
                throw new ArithmeticException("Weight can not be zero.");
            }

            this.weight = weight;
            return this;
        }

        public Builder power(int power) {
            if (power < 0) { 
                throw new ArithmeticException("Wattage can not be zero.");
            }
            
            this.power = power;
            return this;
        }

        public Builder type(String type) throws Exception {
            if (!SpeakersType.isMember(type)) {
                throw new Exception("Invalid type entered.");
            }

            this.type = SpeakersType.valueOf(type);

            return this;
        }

        @Override
        public Speakers build() {
            return new Speakers(this);
        }

        @Override
        protected Builder self() {
            return this;
        }
    }

    private Speakers(Builder builder) {
        super(builder);

        this.weight = builder.weight;
        this.power = builder.power;
        this.type = builder.type;
    }

    @Override
    public String toString() {
        return ("""
                Name: %s
                Power: %d
                Weight: %d
                Type: %s

                SKU %s
                """).formatted(name, power, weight, type, SKU);
    }
    
}
