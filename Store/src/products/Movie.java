package products;

public final class Movie extends Product {

    public enum MovieType {
        DVD, BLUERAY, STEELBOOK, _4K;
        
        /**
         * 
         * Search in all the enum and tries
         * to find if the String entered exists
         * int the enum
         * @param type the member to find
         * @return boolean
         */
        public static boolean isMember(String type) {
            for(MovieType is: MovieType.values()) {
                if (is.name().equals(type)) {
                    return true;
                }
            }

            return false;
        }
    }

    private final String description;
    private final String rating;
    private final MovieType type;
    
    public static class Builder extends Product.Builder<Builder> {
        private String description;
        private String rating;
        private MovieType type;

        public Builder description(String description) {
            if (description == null || description.isEmpty()) {
                throw new IllegalArgumentException("Description must not be empty.");
            }

            this.description = description;

            return this;
        }

        public Builder rating(String rating) {
            if (description == null || description.isEmpty()) {
                throw new IllegalArgumentException("Rating must not be empty.");
            }

            this.rating = rating;

            return this;
        }

        public Builder type(String type) throws Exception {
            if (!MovieType.isMember(type)) {
                throw new Exception("Invalid type entered.");
            }
            
            this.type = MovieType.valueOf(type);
            
            return this;
        }

        // TODO add even more exceptions to the build method
        @Override
        public Movie build() {
            return new Movie(this);
        }

        @Override
        protected Builder self() {
            return this;
        }
    }

    private Movie(Builder builder) {
        super(builder);
        
        this.description = builder.description;
        this.rating = builder.rating;
        this.type = builder.type;
    }

    @Override
    public String toString() {
        return ("""
                Product name: %s.
                Description: %s.
                Format: %s
                Rated: %s.
                SKU: %s.
                """).formatted(name,
                               description,
                               type,
                               rating,
                               SKU);
    }
}