package products;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class Videogame extends Product {

    public enum VideogameConsole {
        XBOX, PS, SWITCH, PC;

        public static boolean isMember(String type) {
            for(VideogameConsole is: VideogameConsole.values()) {
                if (is.name().equals(type)) {
                    return true;
                }
            }

            return false;
        }
    }

    private final String description;
    private final List<String> genre;
    private final String rating;

    public static class Builder extends Product.Builder<Builder> {
        private String description;
        private List<String> genre = new ArrayList<>();
        private String rating;

        public Builder description(String description) {
            if (description == null || description.isEmpty()) {
                throw new IllegalArgumentException("Title must not be empty.");
            }

            this.description = description;

            return this;
        }

        public Builder genre(String genre) {
            if (genre == null || genre.isEmpty()) {
                throw new IllegalArgumentException("Genre field must not be in blank.");
            }

            if (this.genre.contains(genre)) {
                return this;
            }

            this.genre.add(genre);

            return this;
        }

        public Builder rating(String rating) {
            if (rating == null || rating.isEmpty()) {
                throw new IllegalArgumentException("Rating must not be empty.");
            }
            this.rating = rating;

            return this;
        }

         // TODO add even more exceptions to the build method
        @Override
        public Videogame build() {
            return new Videogame(this);
        }

        @Override
        protected Builder self() {
            return this;
        }

    }

    private Videogame(Builder builder) {
        super(builder);
        this.description = builder.description;
        this.genre = Collections.unmodifiableList(builder.genre);
        this.rating = builder.rating;
    }

    @Override
    public String toString() {
        return ("""
                Videogame title: %s
                Description: %s
                Genre: %s
                Rated: %s
                
                SKU: %s
                """).formatted(name, description, genre, rating, SKU);
    }
}