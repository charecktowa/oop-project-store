package products;

public final class Turntable extends Product {

    public enum TurntableType {
        NEW, VINTAGE;

        public static boolean isMember(String type) {
            for(TurntableType is: TurntableType.values()) {
                if (is.name().equals(type)) {
                    return true;
                }
            }

            return false;
        }
    }

    private final int speeds;
    private final TurntableType type;

    public static class Builder extends Product.Builder<Builder> {
        private int speeds;
        private TurntableType type;

        public Builder speeds(int speeds) {
            if (speeds <= 0) {
                throw new ArithmeticException("Speeds cant be zero.");
            }

            this.speeds = speeds;
            return this;
        }

        public Builder type(String type) throws Exception {
            if (!TurntableType.isMember(type)) {
                throw new Exception("Invalid type entered.");
            }

            this.type = TurntableType.valueOf(type);

            return this;
        }

        @Override
        public Turntable build() {
            return new Turntable(this);
        }

        @Override
        protected Builder self() {
            return this;
        }
    }

    private Turntable(Builder builder) {
        super(builder);

        this.speeds = builder.speeds;
        this.type = builder.type;
    }

    @Override
    public String toString() {
        return ("""
                Name: %s
                Speeds (Motor): %d
                Type: %s

                SKU: %s
                """).formatted(name, speeds, type, SKU);
    }
}
