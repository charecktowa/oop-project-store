package products;

public final class Music extends Product {

    public enum MusicType {
        CD, Vinyl, Cassette;

        public static boolean isMember(String type) {
            for(MusicType is: MusicType.values()) {
                if (is.name().equals(type)) {
                    return true;
                }
            }

            return false;
        }
    }

    private final String artistName;
    private final int songs;
    private final int releaseYear;
    private final MusicType type;

    public static class Builder extends Product.Builder<Builder> {
        private String artistName;
        private int songs;
        private int releaseYear;
        private MusicType type;

        public Builder artistName(String artist) {
            if (artist == null || artist.isEmpty()) {
                throw new IllegalArgumentException("Artist name must not be empty.");
            }

            this.artistName = artist;

            return this;
        }

        public Builder songs(int songs) {
            if (songs <= 0) {
                throw new ArithmeticException("Enter a valid quantity of songs.");
            }
            
            this.songs = songs;

            return this;
        }

        public Builder releaseDate(int date) {
            if (date == 0) {
                throw new ArithmeticException("Enter a valid year.");
            }
            this.releaseYear = date;

            return this;
        }

        public Builder type(String type) {
            if (type == null || type.isEmpty()) {
                throw new IllegalArgumentException("Type must not be empty.");
            }
            this.type = MusicType.valueOf(type);

            return this;
        }

         // TODO add even more exceptions to the build method
        @Override
        public Music build() {
            return new Music(this);
        }

        @Override
        protected Builder self() {
            return this;
        }
    }

    private Music(Builder builder) {
        super(builder);
        this.artistName = builder.artistName;
        this.songs = builder.songs;
        this.releaseYear = builder.releaseYear;
        this.type = builder.type;
    }

    @Override
    public String toString() {
        return ("""
                Album Name: %s
                Artist/Band: %s
                Songs: %d
                Type: %s
                Release Year: %d
                
                SKU: %s
                """).formatted(name,
                               artistName,
                               songs,
                               type,
                               releaseYear,
                               SKU);
    }
}
